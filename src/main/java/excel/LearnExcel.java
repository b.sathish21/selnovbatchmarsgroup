package excel;

import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {

	public static Object[][] excelData(String fNme) {
		// TODO Auto-generated method stub

		Object[][] data = null;
			try {
			XSSFWorkbook wb = new XSSFWorkbook("./data/"+fNme+".xlsx");
			XSSFSheet sheet = wb.getSheetAt(0);
			wb.close();
			int rowNum = sheet.getLastRowNum();
			int cellNum = sheet.getRow(0).getLastCellNum();
			data = new Object[rowNum][cellNum];
			for (int i = 1; i <= rowNum; i++) {
				XSSFRow row = sheet.getRow(i);
				for (int j = 0; j < cellNum; j++) {
					XSSFCell cell = row.getCell(j);
					String stringCellValue = cell.getStringCellValue();
					data[i-1][j] = stringCellValue;
					System.out.print(stringCellValue+" ");
				} System.out.println();
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return data;
		
	}

}
