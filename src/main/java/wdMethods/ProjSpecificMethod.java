package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import excel.LearnExcel;

public class ProjSpecificMethod extends SeMethods{
	
	public String fNme;
	
	@DataProvider(name = "fetch")
	public Object[][] getData(){		
		return LearnExcel.excelData(fNme);	
	}
	
	
	@Parameters({"bNme", "url", "uNme", "pwd"})
	@BeforeMethod(groups = "common")
	public void doLogin(String bNme, String url, String uNme, String pwd) {
		startApp(bNme, url);
		type(locateElement("username") , uNme);
		type(locateElement("password") , pwd);
		WebElement login = locateElement("classname", "decorativeSubmit");
		click(login);
		click(locateElement("link", "CRM/SFA"));
	}
	
	@AfterMethod(groups = "common")
		public void closeAPP() {
		closeBrowser();
	}
	
	
}
