package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.UnexpectedTagNameException;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;

public class SeMethods extends ReportGeneration implements WdMethods{
	public RemoteWebDriver driver;	
	public int i = 1;
	ExtentTest test;
	
	
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodrivers.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("The Browser "+browser+" Launched Successfully", "pass");
			//System.out.println("The Browser "+browser+" Launched Successfully");
			
		} catch (TimeoutException e) {
			System.out.println("TimeOut");
			throw new RuntimeException();
		} catch (UnexpectedTagNameException e) {
			System.out.println("Tagname Exception");
			throw new RuntimeException();
		} catch (Exception e) {
			System.out.println("Something Went wrong in StartApp");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}		
	}


	public WebElement locateElement(String locator, String locValue) {

		try {
			switch (locator) {
			case "id":			return driver.findElementById(locValue);
			case "name": 		return driver.findElementByName(locValue);
			case "link": 		return driver.findElementByLinkText(locValue);
			case "classname":   return driver.findElementByClassName(locValue);
			case "xpath": 		return driver.findElementByXPath(locValue);
			}
			return null;
		} catch (NoSuchElementException e) {
			System.out.println("Element not found");
			throw new RuntimeException();
		} catch (UnexpectedTagNameException e) {
			System.out.println("Element not found - Unexpected");
			throw new RuntimeException();
		}catch (Exception e) {
			System.out.println("Something Went wrong in Locating Elemnt using "+locator+" Method.");
			throw new RuntimeException();
		}
	}

	@Override
	public WebElement locateElement(String locValue) {

		try {
			return driver.findElementById(locValue);
		}
		catch (NoSuchElementException e) {
			System.out.println("Element not found");
			throw new RuntimeException();
		} catch (Exception e) {
			System.out.println("Something Went wrong locating element using ID Method.");
			throw new RuntimeException();
		}	 
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportStep("The Data "+data+" Entered Successfully", "pass");
			System.out.println("The Data "+data+" Entered Successfully");
		}
		catch (NoSuchElementException e) {
			System.out.println("Element not found");
			throw new RuntimeException();
		} catch (Exception e) {
			System.out.println("Something Went wrong");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			reportStep("The Element "+ele+" is clicked Successfully", "pass");
			System.out.println("The Element "+ele+" is clicked Successfully");
		}
		catch (NoSuchElementException e) {
			System.out.println("Element not found");
			throw new RuntimeException();
		} catch (Exception e) {
			System.out.println("Something Went wrong before clicking the element");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}	
	}

	
	public void clickWithoutSnap(WebElement ele) {
		try {
			ele.click();
			reportStep("The Element "+ele+" is clicked Successfully", "pass");
			System.out.println("The Element "+ele+" is clicked Successfully");
		}
		catch (NoSuchElementException e) {
			System.out.println("Element not found");
			throw new RuntimeException();
		} catch (Exception e) {
			System.out.println("Something Went wrong before clicking the element");
			throw new RuntimeException();
		}	
	}

	public void waitToClick(WebElement ele) {
		try {
			WebDriverWait wit = new WebDriverWait(driver, 30);
			wit.until(ExpectedConditions.elementToBeClickable(ele));
		}
		catch (NoSuchElementException e) {
			System.out.println("Element not found");
			throw new RuntimeException();
		} catch (ElementNotVisibleException e) {
			System.out.println("Element Not Visible");
			throw new RuntimeException();
		}catch (TimeoutException e) {
			System.out.println("Time Out");
			throw new RuntimeException();
		}catch (Exception e) {
			System.out.println("Something Went wrong during waiting to click the element");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}	
	}


	@Override
	public String getText(WebElement ele) {
		try {
			String text = ele.getText();
			return text;
		}
		catch (NoSuchElementException e) {
			System.out.println("Element not found");
			throw new RuntimeException();
		} catch (Exception e) {
			System.out.println("Something Went wrong");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}
	}


	public void DropDown(String locator, WebElement ele, String value) {

		Select txt = new Select(ele);
		try {
			switch (locator) {
			case "text": txt.selectByVisibleText(value);
			break;
			case "value": txt.selectByValue(value);
			break;
			case "index": txt.selectByIndex(Integer.parseInt(value));
			break;
			}

		} catch (NoSuchElementException e) {
			System.out.println("Element not found");
			throw new RuntimeException();
		} catch (UnexpectedTagNameException e) {
			System.out.println("Element not found");
			throw new RuntimeException();
		}catch (Exception e) {
			System.out.println("Something Went wrong");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub

	}	

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		try {
			String title = driver.getTitle();
			if (title.contains(expectedTitle)) {
				reportStep("Exact Match of "+title+" Found.", "pass");
				System.out.println("Exact Match of "+title+" Found.");
			}
			else {
				reportStep("Match Not Found of "+title+".", "warning");
				System.out.println("Match Not Found of "+title+".");
			}
		}
		catch (NoSuchElementException e) {
			System.err.println("Element Not Found");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.err.println("Something Went Wrong");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		try {
			String text = ele.getText();
			if (text.equalsIgnoreCase(expectedText)) {
				reportStep("Exact Match of "+text+" Found.", "pass");
				System.out.println("Exact Match of "+text+" Found.");
			}
			else {
				reportStep("Match Not Found of "+text+".", "warning");
				System.out.println("Match Not Found of "+text+".");
			}
		}
		catch (NoSuchElementException e) {
			System.err.println("Element Not Found");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.err.println("Something Went Wrong");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			if (text.equalsIgnoreCase(expectedText)) {
				reportStep("Exact Match of "+text+" Found.", "pass");
				System.out.println("Exact Match of "+text+" Found.");
			}
			else {
				reportStep("Match Not Found of "+text+".", "warning");
				System.out.println("Match Not Found of "+text+".");
			}
		}
		catch (NoSuchElementException e) {
			System.err.println("Element Not Found");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.err.println("Something Went Wrong");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		try {
			if(ele.getAttribute(attribute).equals(value)) {
				reportStep("The expected attribute :"+attribute+" value matches the actual "+value, "pass");
				System.out.println("The expected attribute :"+attribute+" value matches the actual "+value);				
			}
			else {
				reportStep("The expected attribute :"+attribute+" value does not matches the actual "+value, "warning");
				System.out.println("The expected attribute :"+attribute+" value does not matches the actual "+value);				
			}
		} 
		catch (NoSuchElementException e) {
			System.err.println("Element Not Found");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.err.println("Something Went Wrong in Eaxct Attribute Test");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

		try {
			if(ele.getAttribute(attribute).contains(value)) {
				reportStep("The expected attribute :"+attribute+" value matches the Partial "+value, "pass");
				System.out.println("The expected attribute :"+attribute+" value matches the Partial "+value);				
			}
			else {
				reportStep("The expected attribute :"+attribute+" value does not matches the Partial "+value, "warning");
				System.out.println("The expected attribute :"+attribute+" value does not matches the Partial "+value);				
			}
		} 
		catch (NoSuchElementException e) {
			System.err.println("Element Not Found");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.err.println("Something Went Wrong in Partial Attribute Test");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub

		try {
			Set<String> wH = driver.getWindowHandles();
			List<String> l = new ArrayList<>();
			l.addAll(wH);
			for (String a : l) {System.out.println(a);}
			driver.switchTo().window(l.get(index));	
		} 
		catch (NoSuchWindowException e) {
			System.err.println("Element Not Found while switching window");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.err.println("Something Went Wrong while switching window");
			throw new RuntimeException();
		}

	}

	@Override
	public void switchToFrame(WebElement ele) {

		try {
			driver.switchTo().frame(ele);
		} 
		catch (NoSuchFrameException e) {
			System.err.println("Frame Not Found");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.err.println("Something Went Wrong while switching to frame");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}

	}

	public void parentFrame() {

		try {
			driver.switchTo().parentFrame();
		} 
		catch (NoSuchFrameException e) {
			System.err.println("Parent frame Not Found");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.err.println("Something Went Wrong while switching to parent frame");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}

	}

	public void defaultFrame() {

		try {
			driver.switchTo().defaultContent();
		} 
		catch (NoSuchFrameException e) {
			System.err.println("Main or default frame Not Found");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.err.println("Something Went Wrong while switching to main or default frame");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
		 	wait.until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().accept();
		} 
		catch (NoAlertPresentException e) {
			System.err.println("Alert Not Present");
			throw new RuntimeException();
		}
		catch (UnhandledAlertException e) {
			System.err.println("Alert is not attended or handled");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.err.println("Something Went Wrong in acepting alert");
			throw new RuntimeException();
		}
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
		 	wait.until(ExpectedConditions.alertIsPresent());
			driver.switchTo().alert().dismiss();
		} 
		catch (NoAlertPresentException e) {
			System.err.println("Alert Not Present");
			throw new RuntimeException();
		}
		catch (UnhandledAlertException e) {
			System.err.println("Alert is not attended or handled");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.err.println("Something Went Wrong in rejecting or dismissing alert");
			throw new RuntimeException();
		}
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
		 	wait.until(ExpectedConditions.alertIsPresent());
			String aText = driver.switchTo().alert().getText();
			return aText;
		} 
		catch (NoAlertPresentException e) {
			System.err.println("Alert Not Present");
			throw new RuntimeException();
		}
		catch (UnhandledAlertException e) {
			System.err.println("Alert is not attended or handled");
			throw new RuntimeException();
		}
		catch (Exception e) {
			System.err.println("Something Went Wrong in getting text from alert");
			throw new RuntimeException();
		}
	}


	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		driver.quit();
	}




}
