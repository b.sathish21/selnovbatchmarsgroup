package wdMethods;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ReportGeneration {

	//To create report
			ExtentReports extent;
			ExtentTest test;
			public String testCase, testCaseDesc, author, category;
			
			
			@BeforeSuite(groups = "common")
			public void beforeSuite() {
				ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/report1.html");//Creating an HTML File	
				html.setAppendExisting(true);
				extent = new ExtentReports();		
				extent.attachReporter(html);//Changing the created HTML file to rewritable.
			}
		
		//Starting Report.
		@BeforeClass
		public void beforeMethod() {
			//TestCase Level
			test = extent.createTest(testCase, testCaseDesc);
			test.assignAuthor(author);
			test.assignCategory(category);
		}
		
		@AfterSuite(groups = "common")
		public void endResult() {
			
			extent.flush();
		}

		public void reportStep(String desc, String status) throws IOException {	
		//Step Level
		if(status.equalsIgnoreCase("PASS")) {
			test.pass(desc);
		}else if(status.equalsIgnoreCase("FAIL")) {
			test.fail(desc);
			throw new RuntimeException();
		}else if(status.equalsIgnoreCase("warning")) {
			test.warning(desc);
		}
	}
}
