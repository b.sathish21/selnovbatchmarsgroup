package newTestCase;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjSpecificMethod;

public class CreateLead extends ProjSpecificMethod{

	//Creating Report for Create Lead
	@BeforeTest
	public void befremthd() {
		 testCase = "Create Lead";
		 testCaseDesc = "Log of Create Lead";
		 author ="Sathish";
		 category = "Smoke";
		 fNme = "CreateLead";
	}
	
	@Test(groups = "smoke", dataProvider = "fetch", timeOut = 120000, priority = 0)
	//@Test(invocationCount = 2, /*timeOut=20000)//*/invocationTimeOut = 20000)
public void createLead(String cNme, String fNme, String lNme) throws InterruptedException {
		
		
		 
		//Entering the Basic Details. 
	    click(locateElement("link", "Create Lead"));
	    
		type(locateElement("createLeadForm_companyName") , cNme);
		type(locateElement("createLeadForm_firstName") , fNme);
		String b = getText(locateElement("createLeadForm_firstName"));
		type(locateElement("createLeadForm_lastName") , lNme);
		
		DropDown("text",locateElement("createLeadForm_dataSourceId"), "Employee");
		DropDown("text",locateElement("createLeadForm_marketingCampaignId") , "Automobile");
		
		type(locateElement("createLeadForm_firstNameLocal"),"Sathish");
		type(locateElement("createLeadForm_lastNameLocal"),"Baskaran");
		type(locateElement("createLeadForm_personalTitle"),"Mr.");
		type(locateElement("createLeadForm_generalProfTitle"),"Lead");
		type(locateElement("createLeadForm_departmentName"),"Electrical");
		type(locateElement("createLeadForm_annualRevenue"),"50000");
		
		DropDown("text",locateElement("createLeadForm_currencyUomId") , "INR - Indian Rupee");
		DropDown("text",locateElement("createLeadForm_industryEnumId") , "Computer Software");
		
		type(locateElement("createLeadForm_numberEmployees"),"500");
		
		DropDown("text",locateElement("createLeadForm_ownershipEnumId") , "S-Corporation");
		
		type(locateElement("createLeadForm_sicCode"),"1990");
		type(locateElement("createLeadForm_tickerSymbol"),"^");
		type(locateElement("createLeadForm_description"),"Description of Sathish Baskaran");
		type(locateElement("createLeadForm_importantNote"),"Important Note about Sathish Baskaran");
		
		//Entering Contact Information.
		locateElement("createLeadForm_primaryPhoneCountryCode").clear();
		
		type(locateElement("createLeadForm_primaryPhoneCountryCode"),"+91");
		type(locateElement("createLeadForm_primaryPhoneAreaCode"),"044");
		type(locateElement("createLeadForm_primaryPhoneNumber"),"25734114");
		type(locateElement("createLeadForm_primaryPhoneExtension"),"301");
		type(locateElement("createLeadForm_primaryPhoneAskForName"),"Thambi");
		type(locateElement("createLeadForm_primaryEmail"),"b.sathish21@gmail.com");
		type(locateElement("createLeadForm_primaryWebUrl"),"http://www.bserv.myhcl.com/");
		
		//Entering Primary Address.
		type(locateElement("createLeadForm_generalToName"),"Sathish");
		type(locateElement("createLeadForm_generalAddress1"),"102C");
		type(locateElement("createLeadForm_generalAddress2"),"Vettri Nagar, Vellanur");
		type(locateElement("createLeadForm_generalCity"),"Chennai");
		DropDown("text",locateElement("createLeadForm_generalCountryGeoId"), "India");
		Thread.sleep(1500);
		DropDown("text",locateElement("createLeadForm_generalStateProvinceGeoId"), "TAMILNADU");
		Thread.sleep(1500);
		type(locateElement("createLeadForm_generalPostalCode"),"600062");
		Thread.sleep(1500);
		click(locateElement("classname","smallSubmit"));
	
		//Verifying the First Name
		String a = locateElement("viewLead_firstName_sp").getText();
		if (a.contains(b)) {System.out.println("Correct");}
		else {System.out.println("Not Correct");}
		Thread.sleep(3000);
	
	}

	/*@DataProvider(name = "fetch")

	public static Object[][] getData(){
		
		LearnExcel.excelData(fNme);
		String[][] data = new String[2][3];
		data[0][0] = "HCL";
		data[0][1] = "Sathish";
		data[0][2] = "Baskaran";
		
		data[1][0] = "IOTL";
		data[1][1] = "Thambi";
		data[1][2] = "B";
		
		return data;
	}*/
	
	@AfterMethod(groups = "common")
	public void closeAPP() {
	closeBrowser();
	}
	
}
